import json
import random
import string
import threading
import Adafruit_DHT
import RPi.GPIO as GPIO
from termcolor import colored
import paho.mqtt.client as mqtt

print('\n')
print('----------------------------------------------------')
print(colored('|                   MQTT SAMPLE 1                  |', 'green', attrs=['bold']))
print('----------------------------------------------------')
print('\n')

GPIO.setmode(GPIO.BCM)
sensor = Adafruit_DHT.DHT11  # Set sensor type: Options are DHT11, DHT22 or AM2302
gpio = 17  # Set GPIO sensor is connected to
relay = 18  # Set GPIO Relay is connected to
GPIO.setup(relay, GPIO.OUT)  # Setup Mode of GPIO

DEVICE_ID = 'f25b5c40-31a3-46e7-a900-7b2ba1c20f92'  # must change to your deviceId
CLIENT_ID = '23SJ6MGM5TONQK4EJLY06P6'  # must change to your clientId
MQTT_SERVER = '172.16.110.79'  # Podiot Mqtt Broker Address (mqtt.thingscloud.ir)
MQTT_PORT = int('1883')  # Podiot Mqtt Broker Port (1883)

# Change this appropiately
SENSOR_NAME_1 = 'temperature'
SENSOR_NAME_2 = 'humidity'
Request_ID = '$requestId'
Version = '$version'

# global variable
desiredVersion_last = ''
get_requestId_desired = ''
reportedVersion_last = ''
nextReport = False
timeout = True

# publish topics
publishTopicReported = 'dvcasy/twin/update/reported'
publishTopicGet = 'dvcasy/twin/get'
publishTopicDelete = 'dvcasy/twin/delete'

# subscribe topics
subscribeTopicOrigin = 'dvcout/' + DEVICE_ID + '/' + CLIENT_ID + '/#'
subscribeTopicDesired = 'dvcout/' + DEVICE_ID + '/' + CLIENT_ID + '/twin/update/desired'
subscribeTopicDocument = 'dvcout/' + DEVICE_ID + '/' + CLIENT_ID + '/twin/update/document'
subscribeTopicAccepted = 'dvcout/' + DEVICE_ID + '/' + CLIENT_ID + '/twin/response/accepted'
subscribeTopicRejected = 'dvcout/' + DEVICE_ID + '/' + CLIENT_ID + '/twin/response/rejected'


class Payload(object):
    def __init__(self, msg):
        self.__dict__ = json.loads(msg)


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected to Podiot with result code " + str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(subscribeTopicOrigin)
    # if connection successful start publishing data
    if rc == 0:
        publishRequestGet('GetForRetrieve$versions')
        publishTempData()


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    
    global nextReport
    global reportedVersion_last
    
    json_msg = Payload(msg.payload)
    content = json_msg.content
    main_msg = json.loads(content)
    json_object = json.loads(str(msg.payload.decode()))
    json_object['content'] = json.loads(str(json_object['content']))
    json_formatted_str = json.dumps(json_object, indent=2)
    

    if msg.topic == subscribeTopicDesired:
        print(colored('\nMessage Received from DesiredTopic:', 'red', attrs=['bold']), json_formatted_str)
        desiredVersion = main_msg['deviceTwinDocument']['attributes']['desired']['$version']
        print(colored(f'\n$version in received desired update message is: {desiredVersion}', 'blue', attrs=['bold']))
        global desiredVersion_last
        print(colored(f'\nlast $version was: {desiredVersion_last}', 'blue', attrs=['bold']))

        if desiredVersion >= desiredVersion_last:
            print(colored(
                '\nOk! this desired update message has $version equal or upper than last message performed on device, now update device state base it on!',
                'magenta', attrs=['bold']))

            desiredVersion_last = desiredVersion
            command = main_msg['deviceTwinDocument']['attributes']['desired']['airCondition']
            print(f'command is : {command}')
            if command == 'on':
                GPIO.output(relay, GPIO.HIGH)
            if command == 'off':
                GPIO.output(relay, GPIO.LOW)
        else:
            print(colored(
                '\nNot OK! this desired update message has $version lower than last message performed on device, so can decide not to act on a received message!',
                'magenta', attrs=['bold']))

    if msg.topic == subscribeTopicDocument:
        print(colored('\nMessage Received from DocumentTopic:', 'red', attrs=['bold']), json_formatted_str)

    if msg.topic == subscribeTopicAccepted:

        if main_msg['$requestId'] == 'GetForRetrieve$versions':
            desiredVersion_last = main_msg['deviceTwinDocument']['attributes']['desired']['$version']
            reportedVersion_last = main_msg['deviceTwinDocument']['attributes']['reported']['$version']
            print(colored(f'\nlast desired $version in device twin document is: {desiredVersion_last}', 'blue',
                          attrs=['bold']))
            print(colored(f'last reported $version in device twin document is: {reportedVersion_last}', 'blue',
                          attrs=['bold']))
            
        if main_msg['$requestId'] == 'GetForLastReported$Version':
            reportedVersion = main_msg['deviceTwinDocument']['attributes']['reported']['$version']
            print(colored(
                f'\nlast reported $version in device twin document after timeout occurred is: {reportedVersion}',
                'blue',
                attrs=['bold']))
            nextReport = True
            timer1.cancel()

            if reportedVersion == reportedVersion_last:
                print(colored(
                    '\nreported update message didnt update on Podiot, lets publish again',
                    'gray',
                    attrs=['bold']))
            if reportedVersion == reportedVersion_last + 1:
                print(colored(
                    '\nreported update message updated on Podiot, but no received any accepted or rejected from Podiot',
                    'gray',
                    attrs=['bold']))
                reportedVersion_last = reportedVersion

        if main_msg['$requestId'] == reported_requestId:
            print(colored('\nMessage Received from AcceptedTopic for reported update:', 'red', attrs=['bold']), str(
                msg.payload))
            print(colored((
                              '\nreported update message updated successfully on Podiot, now everything ready for send next reported update message').upper(),
                          'cyan',
                          attrs=['bold']))
            nextReport = True
            timer2.cancel()
            reportedVersion = main_msg['deviceTwinDocument']['attributes']['reported']['$version']
            if reportedVersion_last < reportedVersion:
                reportedVersion_last = reportedVersion
            print(colored(
                f'last reported $version in device twin document after reported update is: {reportedVersion_last}',
                'cyan', attrs=['bold']))

    if msg.topic == subscribeTopicRejected:
        print(colored("\nMessage Received from RejectedTopic: "'red', attrs=['bold']) + str(msg.payload))
        json_msg = Payload(msg.payload)
        content = json_msg.content
        main_msg = json.loads(content)

        if main_msg['$requestId'] == 'GetForRetrieve$versions' | main_msg['$requestId'] == 'GetForLastReported$Version':
            desiredVersion_last = 0
            reportedVersion_last = 0
            nextReport = True
            timer1.cancel()

        if main_msg['$requestId'] == reported_requestId:
            nextReport = True
            timer2.cancel()
            print(
                colored(
                    '\nreported update message didnt update on Podiot, detect error message for more details, then decide if resend corrected reported update message or cancle',
                    'gray',
                    attrs=['bold']))


# Publish random temp data between 10-100
def publishData():
    global reported_requestId
    reported_requestId = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
    # Use read_retry method. This will retry up to 15 times to
    # get a sensor reading (waiting 2 seconds between each retry).
    humidity, temperature = Adafruit_DHT.read_retry(sensor, gpio)
    message = json.dumps({Request_ID: reported_requestId, "deviceTwinDocument": {
        "attributes": {"reported": {SENSOR_NAME_1: str(temperature), SENSOR_NAME_2: str(humidity),
                                   Version: reportedVersion_last}}}}, indent=2)
    print(colored(f'Publish Data: {message}', 'yellow', attrs=['bold']))
    client.publish(publishTopicReported, message)


def publishRequestGet(get_requestId):
    message = json.dumps({Request_ID: get_requestId})
    print(colored(f'\n\nPublish Request Get: {message}', 'blue', attrs=['bold']))
    client.publish(publishTopicGet, message)


def setTimeout():
    global timeout, nextReport
    timeout = True
    nextReport = False
    print(colored(
        f'\nTime Out! no received any response from Podiot on Accepted or Rejected Topic. So check for $version on Podiot',
        'red', attrs=['bold']))


def publishTempData():
    if reportedVersion_last != '':
        global nextReport
        global timeout, timer1, timer2
        if nextReport | timeout:
            print('\ntimeout occurred:', str(timeout))
        if timeout:
            publishRequestGet("GetForLastReported$Version")
            nextReport = False
            timeout = False
            timer1 = threading.Timer(5, setTimeout)
            timer1.setName('timeoutTimer1')
            timer1.start()
        if nextReport:
            publishData()
            nextReport = False
            timeout = False
            timer2 = threading.Timer(5, setTimeout)
            timer2.setName('timeoutTimer2')
            timer2.start()
    timer3 = threading.Timer(10, publishTempData)
    timer3.setName('publishTimer')
    timer3.start()


client = mqtt.Client(CLIENT_ID)
client.on_connect = on_connect
client.on_message = on_message
client.connect(MQTT_SERVER, MQTT_PORT, 60, bind_address="")
client.loop_forever()
